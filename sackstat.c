#include <asm/msr.h>
#include <asm/unistd.h>
#include <linux/sched.h>
#include <linux/sackstat.h>
#include <linux/slab.h>
#include <linux/cpuset.h>

int bitCount(unsigned int u)
{
	unsigned int uCount;
	uCount = u - ((u >> 1) & 033333333333) - ((u >> 2) & 011111111111);
	return ((uCount + (uCount >> 3)) & 030707070707) % 63;
}

void read_cpu_counters(struct task_struct *current_task , int current_cpu)
{
	uint16_t tempWeight;

	struct task_struct *p;

	struct cpumask mask;

	p = current_task;

	if(p->lbalancing.context_switches >= 100 && p->lbalancing.context_switches % 10 == 0)
	{
		if(p->lbalancing.instructions_retired > 0 && p->lbalancing.memory_ops > 0 && (p->lbalancing.instructions_retired/p->lbalancing.memory_ops) <= 3 ) // && p->lbalancing.llc_references > 0 && (p->lbalancing.memory_ops/(p->lbalancing.llc_references+p->lbalancing.llc_misses)) <= 13)
		{
			if(p->lbalancing.context_switches == 100)
			{
				p->lbalancing.weight_p = 63;
				p->lbalancing.migratable = 1;
	          		cpumask_clear(&mask);
        	       	        if(current_cpu > 20)
               		        {
                       			current_cpu = current_cpu - 20;
                       		}
                       		cpumask_set_cpu(current_cpu, &mask);
                		cpumask_set_cpu(current_cpu + 20, &mask);
       	        		cpumask_copy(&p->cpus_allowed,&mask);
      	        		p->lbalancing.maskSet = 1;
			}
			else
			{
				tempWeight = p->lbalancing.weight_p;
				tempWeight = tempWeight << 1;
				tempWeight+=1;
				p->lbalancing.weight_p = tempWeight;
			}

			if(p->lbalancing.llc_references > 0 && p->lbalancing.llc_misses > 0 && (p->lbalancing.memory_ops/(p->lbalancing.llc_references+p->lbalancing.llc_misses)) <= 13)
			{
	 			 if(p->lbalancing.context_switches == 100)
                        	{
  					p->lbalancing.migratable = 2;
                               		p->lbalancing.weight_l2 = 63;
                        	}
				else
				{
	                                tempWeight = p->lbalancing.weight_l2;
        	                       	tempWeight = tempWeight << 1;
       				        tempWeight+=1;
                        	        p->lbalancing.weight_l2 = tempWeight;
				}
			}
			else
			{
				tempWeight = p->lbalancing.weight_l2;
                               	tempWeight = tempWeight << 1;
                                p->lbalancing.weight_l2 = tempWeight;

			}

			p->lbalancing.memory_trigger_count++;

			if(bitCount(p->lbalancing.weight_p) >= 8)
			{
				p->lbalancing.memory_phase_count++;
				p->lbalancing.migratable = 1;
				if(bitCount(p->lbalancing.weight_l2) >= 8)
				{
					p->lbalancing.l2_phase_count++;
					p->lbalancing.migratable = 2;
				}

				if(p->lbalancing.maskSet == 0)
				{
	        	  		cpumask_clear(&mask);
        	       		        if(current_cpu > 20)
               		        	{
                       				current_cpu = current_cpu - 20;
                       			}
                       			cpumask_set_cpu(current_cpu, &mask);
                			cpumask_set_cpu(current_cpu + 20, &mask);
       	        			cpumask_copy(&p->cpus_allowed,&mask);
      	        			p->lbalancing.maskSet = 1;
      				}
			}


		}
		else
		{
			if(p->lbalancing.context_switches == 100)
			{
				p->lbalancing.weight_p = 31;
				p->lbalancing.maskSet = 0;
			}
			else
			{
				tempWeight = p->lbalancing.weight_p;
				tempWeight = tempWeight << 1;
				p->lbalancing.weight_p = tempWeight;
			}

			p->lbalancing.core_trigger_count++;

			if(bitCount(p->lbalancing.weight_p) < 8)
			{
				p->lbalancing.core_phase_count++;
				p->lbalancing.migratable = 0;
				if(p->lbalancing.maskSet == 1)
				{
                			cpumask_clear(&mask);
					cpumask_setall(&mask);
           		     	        cpumask_copy(&p->cpus_allowed,&mask);
                		        p->lbalancing.maskSet = 0;
				}
			}

			tempWeight = p->lbalancing.weight_l2;
                       	tempWeight = tempWeight << 1;
                        p->lbalancing.weight_l2 = tempWeight;
		}

		p->lbalancing.instructions_retired_saved += p->lbalancing.instructions_retired;
		p->lbalancing.total_cycles_saved += p->lbalancing.total_cycles;
		p->lbalancing.memory_ops_saved += p->lbalancing.memory_ops;
		p->lbalancing.llc_references_saved += p->lbalancing.llc_references;
		p->lbalancing.llc_misses_saved += p->lbalancing.llc_misses;

		p->lbalancing.instructions_retired = native_read_msr(IA32_FIXED_CTR0);
		p->lbalancing.total_cycles = native_read_msr(IA32_FIXED_CTR1);
		p->lbalancing.memory_ops = native_read_msr(IA32_PMC0);
		p->lbalancing.memory_ops += native_read_msr(IA32_PMC1);
		p->lbalancing.llc_references = native_read_msr(IA32_PMC2);
		p->lbalancing.llc_misses = native_read_msr(IA32_PMC3);
	}
	else
	{
		p->lbalancing.instructions_retired += native_read_msr(IA32_FIXED_CTR0);
		p->lbalancing.total_cycles += native_read_msr(IA32_FIXED_CTR1);
		p->lbalancing.memory_ops += native_read_msr(IA32_PMC0);
		p->lbalancing.memory_ops += native_read_msr(IA32_PMC1);
		p->lbalancing.llc_references += native_read_msr(IA32_PMC2);
		p->lbalancing.llc_misses += native_read_msr(IA32_PMC3);
	}

	if(p->lbalancing.instructions_retired == 0)
	{
		p->lbalancing.context_switches = 0;
		p->lbalancing.stats_collected = 0;
		p->lbalancing.context_switches_current_cpu = 0;
		p->lbalancing.cpu = -1;
		p->lbalancing.migrations = 0;
		return;
	}

	p->lbalancing.context_switches++;

	if(p->lbalancing.cpu == current_cpu || p->lbalancing.cpu == current_cpu + 20)
        {
                p->lbalancing.context_switches_current_cpu++;
        }
        else
        {
                p->lbalancing.context_switches_current_cpu = 0;
                p->lbalancing.migrations++;
        }

        p->lbalancing.cpu = current_cpu;

	if(p->lbalancing.context_switches >= 100 && p->lbalancing.stats_collected == 0 )
	{
		p->lbalancing.stats_collected = 1;
	}
	return;
}

void reset_cpu_counters(void)
{
	native_write_msr(IA32_FIXED_CTR0, NONE, NONE);
	native_write_msr(IA32_FIXED_CTR1, NONE, NONE);
	native_write_msr(IA32_PMC0, NONE, NONE);
	native_write_msr(IA32_PMC1, NONE, NONE);
	native_write_msr(IA32_PMC2, NONE, NONE);
	native_write_msr(IA32_PMC3, NONE, NONE);
}
