MAKEFLAGS=--no-print-directory
KDIR := /lib/modules/$(shell uname -r)/build
MDIR := $(shell pwd)
PWD := $(shell pwd)
obj-m += sackmsr.o
sackstat-objs += sackmsr.o

module:
	make -C $(KDIR) SUBDIRS=$(PWD) modules

.PHONY: clean
clean:
	@make -C $(KDIR) SUBDIRS=$(PWD) clean

