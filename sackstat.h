#ifndef _INCLUDE_SACKSTAT_H
#define _INCLUDE_SACKSTAT_H

#include <linux/sched.h>
/* Define addresses for performance counter values/controls.
 * Addresses of IA32_PERFEVTSELx/IA32_PMCx remain the same across
 * microarchitectures.  Refer to Intel® 64 and IA-32 Architectures
 * Software Developer’s Manual Volume 3, Chapter 18 for additional
 * information.
 */
#define         IA32_PERFEVTSEL0        0x186
#define         IA32_PERFEVTSEL1        0x187
#define         IA32_PERFEVTSEL2        0x188
#define         IA32_PERFEVTSEL3        0x189
#define         IA32_FIXED_CTRL         0x38D
#define         IA32_FIXED_STATUS       0x38E
#define         IA32_FIXED_CTR0         0x309
#define         IA32_FIXED_CTR1         0x30A
#define         IA32_FIXED_CTR2         0x30B
#define         IA32_PMC0               0xC1
#define         IA32_PMC1               0xC2
#define         IA32_PMC2               0xC3
#define         IA32_PMC3               0xC4
#define         NONE                    0x0

struct tasks_info
{
	int weight;
	unsigned int current_cpu;
	unsigned int new_cpu;
	unsigned int migratable;
	unsigned int need_resched;
	struct task_struct* lb_task_struct;
	struct tasks_info* next;
};

struct logical_core_info
{
	signed int 	 	      total;
	signed int 	    	      mem_total;
	signed int                    mem_tasks;
	signed int                    l3_tasks;
	signed int                    l2_tasks;
	signed int 		      core_tasks;
	unsigned int 		      need_resched;
	struct tasks_info* task_list_memBound;
	struct tasks_info* task_list_l3Bound;
	struct tasks_info* task_list_l2Bound;
	struct tasks_info* task_list_coreBound;
	//struct tasks_info* task_list_need_resched;
};

struct socket_info
{
	signed int                    total;
	signed int                    mem_total;
    signed int                    mem_tasks;
	signed int                    l3_tasks;
	signed int                    l2_tasks;
	signed int 		      		  core_tasks;
};

extern int collecting;

extern int distributing;

extern struct logical_core_info logical_cores_info[24];

extern struct socket_info sockets_info[2];

void read_cpu_counters(struct task_struct *current_task , int current_cpu);

void reset_cpu_counters(void);


#endif
