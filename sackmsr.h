#ifndef _INCLUDE_SACKMSR_H
#define _INCLUDE_SACKMSR_H

#include <linux/sched.h>
/* Define addresses for performance counter values/controls.
 * Addresses of IA32_PERFEVTSELx/IA32_PMCx remain the same across 
 * microarchitectures.  Refer to Intel® 64 and IA-32 Architectures
 * Software Developer’s Manual Volume 3, Chapter 18 for additional
 * information.
 */
#define 	IA32_PERFEVTSEL0 	0x186
#define 	IA32_PERFEVTSEL1 	0x187
#define 	IA32_PERFEVTSEL2 	0x188
#define 	IA32_PERFEVTSEL3 	0x189
#define 	IA32_FIXED_CTRL		0x38D
#define 	IA32_FIXED_STATUS 	0x38E
#define 	IA32_FIXED_CTR0		0x309
#define 	IA32_FIXED_CTR1		0x30A
#define 	IA32_FIXED_CTR2		0x30B
#define 	IA32_PMC0			0xC1
#define 	IA32_PMC1 			0xC2
#define 	IA32_PMC2 			0xC3
#define 	IA32_PMC3			0xC4
#define 	NONE 				0x0
#define 	DEFAULT_MASK		0x00530000
#define		APM_INFO			0x0000000A

#define writemsr(address, value) 	\
	__asm__ __volatile__ 		\
	("movl %0, %%ecx;		\
	movl %1,%%eax; 			\
	movl $0,%%edx; 			\
	wrmsr;" 			\
	:        		 	\
	:"r"(address),"r" (value)       \
	:"%eax","%edx","%ecx")          \

#define readpmc(counterno, value) 	\
	__asm__ __volatile__ 		\
	("movl %0, %%ecx; 		\
	rdpmc; 				\
	movl %%eax, %1;" 		\
	:"=r" (value) 			\
	:"r" (counterno) 		\
	:"%eax", "%edx", "%ecx") 	\


#define cpuid(function, eax, ebx, ecx, edx)		\
	__asm__ __volatile__ 				\
	("cpuid": "=a" 					\
	(eax),"=b" 					\
	(ebx),"=c" 					\
	(ecx),"=d" 					\
	(edx): "a" (function)) 				\

#define AND_RIGHT(value) 				\
	 ((1 << (value)) - 1)			

#define SHIFT_LEFT(value, begin, end) 			\
	(((value & AND_RIGHT(end)) >> begin))

// Handle special cases where 0 is actually true:
#define O_SUPPORT(value) 				\
	((value < 1) ? "Supported" : "Not Supported")

struct tasks_info
{
	int 			weight;
	unsigned int 		current_cpu;
	unsigned int 		new_cpu;
	unsigned int 		migratable;
	unsigned int 		need_resched;
	struct task_struct* 	lb_task_struct;
	struct tasks_info*  	next;
};

/*
struct logical_core_info
{
	signed int 	 	      	  total;
	signed int 	    	          mem_total;
	signed int                        mem_tasks;
	signed int                        l3_tasks;
	signed int                        l2_tasks;
	signed int 		          core_tasks;
	unsigned int 		          need_resched;
	struct tasks_info* 		  task_list_memBound;
	struct tasks_info* 		  task_list_l3Bound;
	struct tasks_info* 		  task_list_l2Bound;
	struct tasks_info* 		  task_list_pre_coreBound;
	struct tasks_info*              task_list_need_resched;
};*/

struct socket_info
{
	signed int                total;
	signed int                mem_total;
	signed int                mem_tasks;
	signed int                l3_tasks;
	signed int                l2_tasks;
	signed int                l1_tasks;
	signed int 		  core_tasks;
};

struct physical_core_info
{
	signed int		      		total;
	signed int 		     		mem_total;
    signed int            			mem_tasks;
	signed int            			l3_tasks;
	signed int            			l2_tasks;
	signed int 				l1_tasks;
	signed int 		      		core_tasks;
	struct tasks_info* 		  	task_list_memBound;
	struct tasks_info* 		  	task_list_l3Bound;
	struct tasks_info* 		  	task_list_l2Bound;
	struct tasks_info*			task_list_l1Bound;
	struct tasks_info* 		  	task_list_coreBound;
	//signed int 		      need_resched;
};

//struct logical_core_info logical_cores_info[24];

struct socket_info sockets_info[2];

struct physical_core_info physical_cores_list[20];

void print_stats(void);

void sackstat_resched(void);

void seperate_load(void);

void balance_load(void);

void migrate_threads(void);

void dealloc_lists(void);

#endif
