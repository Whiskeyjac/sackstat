#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros
#include <asm/current.h>
#include <asm/msr.h>
#include <asm/unistd.h>
#include <linux/sched.h>
#include <linux/sackstat.h>
#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/slab.h>
#include "sackmsr.h"
#include <linux/rcupdate.h>
#include <linux/sched/task.h>
#include <linux/sched/signal.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ravi");
MODULE_DESCRIPTION("A kernel module to initialize MSR and create a background thread for Load Balancing");

static struct task_struct *thread_st;

int sleep_time = 2;

void sackstat_resched(void)
{
	struct task_struct *g, *currTask;

	int current_cpu;

	struct tasks_info *temp, *curr;
	rcu_read_lock();
/*	for_each_process_thread(g,currTask)
	{
		if(currTask->state == TASK_RUNNING && !(currTask->flags & PF_KTHREAD) && !(currTask->flags & PF_WQ_WORKER) && !(currTask->flags & PF_NO_SETAFFINITY) && currTask->lbalancing.stats_collected == 1)
		{
			printk(KERN_INFO "Task_Name:%-15s  Task_ID:%5u  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t LLC:%llu Misses:%llu Swit:%llu  Mig:%d  MemTrig:%d  MemClass:%d  CoreTrig:%d  CoreClass:%d\n",currTask->comm, currTask->pid, currTask->lbalancing.instructions_retired_saved, currTask->lbalancing.total_cycles_saved, currTask->lbalancing.memory_ops_saved, currTask->lbalancing.llc_references_saved, currTask->lbalancing.llc_misses_saved, currTask->lbalancing.context_switches, currTask->lbalancing.migrations, currTask->lbalancing.memory_trigger_count, currTask->lbalancing.memory_phase_count, currTask->lbalancing.core_trigger_count, currTask->lbalancing.core_phase_count);
		}
	}
	rcu_read_unlock();
*/
	for_each_process_thread(g,currTask)
	{
		get_task_struct(currTask);
		//printk(KERN_INFO "Task_Name:%-15s  Task_ID:%5u\n",currTask->comm, currTask->pid);
		//if(currTask->state == TASK_RUNNING && currTask->lbalancing.stats_collected == 1 && !(currTask->flags & PF_KTHREAD) && !(currTask->flags & PF_WQ_WORKER) && !(currTask->flags & PF_NO_SETAFFINITY))
		if(currTask->state == TASK_RUNNING && !(currTask->flags & PF_KTHREAD) && !(currTask->flags & PF_WQ_WORKER) && !(currTask->flags & PF_NO_SETAFFINITY) && currTask->lbalancing.stats_collected == 1)
		{
			current_cpu = currTask->cpu;
			printk(KERN_INFO "Task_Name:%-15s  Task_ID:%5u CPU:%d \n",currTask->comm, currTask->pid ,current_cpu);
			sockets_info[current_cpu % 2].total++;
			physical_cores_list[current_cpu % 20].total++;

			if(currTask->lbalancing.migratable == 3)
			{
				sockets_info[current_cpu % 2].mem_total++;
				sockets_info[current_cpu % 2].l3_tasks++;
				physical_cores_list[current_cpu % 20].mem_total++;
				physical_cores_list[current_cpu % 20].l3_tasks++;
				temp = physical_cores_list[current_cpu % 20].task_list_l3Bound;
			}
			else if(currTask->lbalancing.migratable == 2)
			{
				sockets_info[current_cpu % 2].mem_total++;
				sockets_info[current_cpu % 2].l2_tasks++;
				physical_cores_list[current_cpu % 20].mem_total++;
				physical_cores_list[current_cpu % 20].l2_tasks++;
				temp = physical_cores_list[current_cpu % 20].task_list_l2Bound;
			}
			else if(currTask->lbalancing.migratable == 1)
			{
				sockets_info[current_cpu % 2].mem_total++;
				sockets_info[current_cpu % 2].l1_tasks++;
				physical_cores_list[current_cpu % 20].mem_total++;
				physical_cores_list[current_cpu % 20].l1_tasks++;
				temp = physical_cores_list[current_cpu % 20].task_list_l1Bound;
			}
			else
			{
				sockets_info[current_cpu % 2].core_tasks++;
				physical_cores_list[current_cpu % 20].core_tasks++;
				temp = physical_cores_list[current_cpu % 20].task_list_coreBound;
			}

			curr =  kmalloc(sizeof(struct tasks_info), GFP_ATOMIC);

			if( curr == NULL)
			{
				printk(KERN_INFO "%s:%d: Failed\n", __func__, __LINE__ );
			}

			curr->weight = currTask->lbalancing.weight_p;
			curr->current_cpu = current_cpu;
			curr->lb_task_struct = currTask;
			curr->migratable = currTask->lbalancing.migratable;
			curr->next = NULL;

			if(curr->migratable == 3)
			{
				physical_cores_list[current_cpu % 20].task_list_l3Bound = curr;
				curr->next = temp;
			}
			else if(curr->migratable == 2)
			{
				physical_cores_list[current_cpu % 20].task_list_l2Bound = curr;
				curr->next = temp;
			}
			else if(curr->migratable == 1)
			{
				physical_cores_list[current_cpu % 20].task_list_l1Bound = curr;
				curr->next = temp;
			}
			else
			{
				physical_cores_list[current_cpu % 20].task_list_coreBound = curr;
				curr->next = temp;
			}

		}
		else
		{
			put_task_struct(currTask);
		}
	}

	rcu_read_unlock();

	if(sockets_info[0].mem_total <= 1)
	{
		//printk(KERN_INFO "Returning\n");
		seperate_load();
		//print_stats();
//		dealloc_lists();
		return;
	}
	else if(sockets_info[0].mem_total > 1 && sockets_info[0].mem_total <= 10)
	{
		//printk(KERN_INFO "Seperating Tasks\n");
		//print_stats();
		seperate_load();
//		dealloc_lists();
	}
	else if(sockets_info[0].mem_total > 10 && sockets_info[0].mem_total <= 20)
	{
		//balance_load();
		//printk(KERN_INFO "Balancing Tasks\n");
		print_stats();
		dealloc_lists();
	}
	else
	{
		printk(KERN_INFO "Total Mem Count %d\n",sockets_info[0].mem_total);
		dealloc_lists();
	}

//	print_stats();
	return;
}

void print_stats(void)
{
	int i;

	struct tasks_info *currTask = NULL;

	struct task_struct *tempTask;

	for(i =0; i < 20; i+=2)
	{

		printk(KERN_INFO "Sackstat ******* Start ********\n\n\n");
		currTask = physical_cores_list[i].task_list_memBound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : Memory Task ->Task_Name:%-15s  Task_ID:%5u  cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t L2-Loads:%llu L2-Stores:%llu Swit:%llu  Mig:%d  MemTrig:%d  L1Class:%d L2Class:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.l2_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		currTask = physical_cores_list[i].task_list_l3Bound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : L3 Task ->Task_Name:%-15s  Task_ID:%5u  cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t L2-Loads:%llu L2-Stores:%llu Swit:%llu  Mig:%d  MemTrig:%d  L1Class:%d L2Class:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.l2_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		currTask = physical_cores_list[i].task_list_l2Bound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : L2 Task ->Task_Name:%-15s  Task_ID:%5u  cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t L2-Loads:%llu L2-Stores:%llu Swit:%llu  Mig:%d  MemTrig:%d  L1Class:%d L2Class:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.l2_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		currTask = physical_cores_list[i].task_list_l1Bound;

                while(currTask != NULL)
                {
                        tempTask = currTask->lb_task_struct;
                        if(tempTask != NULL && tempTask->state == TASK_RUNNING)
                        {
				printk(KERN_INFO "Sackstat : L1 Task ->Task_Name:%-15s  Task_ID:%5u  cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t L2-Loads:%llu L2-Stores:%llu Swit:%llu  Mig:%d  MemTrig:%d  L1Class:%d L2Class:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.l2_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
                        }
                        currTask = currTask->next;
                }


		currTask = physical_cores_list[i].task_list_coreBound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : Core Task ->Task_Name:%-15s  Task_ID:%5u  cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t L2-Loads:%llu L2-Stores:%llu Swit:%llu  Mig:%d  MemTrig:%d  L1Class:%d L2Class:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.l2_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		printk(KERN_INFO "\n\n\n Sackstat ******** End ********\n\n\n");

		//printk(KERN_INFO "Sackstat : Total Core Bound in Physical Core in Core %d ->  %d \n",i,physical_cores_list[i].core_tasks);
	}

	dealloc_lists();
	return;
}

void seperate_load(void)
{
	int i , j ;

	int type=0,currCpu=0;

	struct tasks_info *currTask = NULL;

//	struct tasks_info *headTask = NULL;

	struct task_struct *tempTask;

	struct cpumask mask;

//	long g;

	for(i =0; i < 20; i+=2)
	{

		printk(KERN_INFO "Sackstat ******* Start ********\n\n\n");
		currTask = physical_cores_list[i].task_list_memBound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : Memory Task ->Task_Name:%-15s  Task_ID:%5u  cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t L2-Loads:%llu L2-Stores:%llu Swit:%llu  Mig:%d  MemTrig:%d  L1Class:%d L2Class:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.l2_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		currTask = physical_cores_list[i].task_list_l3Bound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : L3 Task ->Task_Name:%-15s  Task_ID:%5u  cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t L2-Loads:%llu L2-Stores:%llu Swit:%llu  Mig:%d  MemTrig:%d  L1Class:%d L2Class:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.l2_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		currTask = physical_cores_list[i].task_list_l2Bound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : L2 Task ->Task_Name:%-15s  Task_ID:%5u  cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t L2-Loads:%llu L2-Stores:%llu Swit:%llu  Mig:%d  MemTrig:%d  L1Class:%d L2Class:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.l2_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		currTask = physical_cores_list[i].task_list_l1Bound;

                while(currTask != NULL)
                {
                        tempTask = currTask->lb_task_struct;
                        if(tempTask != NULL && tempTask->state == TASK_RUNNING)
                        {
				printk(KERN_INFO "Sackstat : L1 Task ->Task_Name:%-15s  Task_ID:%5u  cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t L2-Loads:%llu L2-Stores:%llu Swit:%llu  Mig:%d  MemTrig:%d  L1Class:%d L2Class:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.l2_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
                        }
                        currTask = currTask->next;
                }

		currTask = physical_cores_list[i].task_list_coreBound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : Core Task ->Task_Name:%-15s  Task_ID:%5u  cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t L2-Loads:%llu L2-Stores:%llu Swit:%llu  Mig:%d  MemTrig:%d  L1Class:%d L2Class:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.l2_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		printk(KERN_INFO "\n\n\n Sackstat ******** End ********\n\n\n");

		//printk(KERN_INFO "Sackstat : Total Core Bound in Physical Core in Core %d ->  %d \n",i,physical_cores_list[i].core_tasks);
	}

	for(i = 0; i < 20; i+=2)
	{

		currTask = NULL;
		tempTask = NULL;

		while(physical_cores_list[i].mem_total > 1)
		{
			if(physical_cores_list[i].mem_tasks > 0 )
			{
				currTask = physical_cores_list[i].task_list_memBound;
				physical_cores_list[i].task_list_memBound = currTask->next;
				physical_cores_list[i].mem_tasks--;
				currCpu = i;
				type = 3;
			}
			else if(physical_cores_list[i].l3_tasks > 0)
			{
				currTask = physical_cores_list[i].task_list_l3Bound;
				physical_cores_list[i].task_list_l3Bound = currTask->next;
				physical_cores_list[i].l3_tasks--;
				currCpu = i;
				type = 2;
			}
			else if(physical_cores_list[i].l1_tasks > 0)
                        {
                                currTask = physical_cores_list[i].task_list_l1Bound;
                                physical_cores_list[i].task_list_l1Bound = currTask->next;
                                physical_cores_list[i].l1_tasks--;
                                currCpu = i;
                                type = 1;
                        }
			else if(physical_cores_list[i].l2_tasks > 0)
			{
				currTask = physical_cores_list[i].task_list_l2Bound;
				physical_cores_list[i].task_list_l2Bound = currTask->next;
				physical_cores_list[i].l2_tasks--;
				currCpu = i;
				type = 1;
			}

			physical_cores_list[i].mem_total--;
			physical_cores_list[i].total--;

			tempTask = currTask->lb_task_struct;

			cpumask_clear(&mask);

			for(j = 0 ; j < 20; j+=2)
			{
				if(i == j)
				{
					continue;
				}

				if(physical_cores_list[j].mem_total == 0)
				{
					if(tempTask == NULL || tempTask->state != TASK_RUNNING)
					{
						put_task_struct(tempTask);
						if(currTask != NULL)
						{
							kfree(currTask);
							currTask = NULL;
						}
						break;
					}
					if(type == 3)
					{
						physical_cores_list[j].l3_tasks++;
					}
					else if(type == 1)
					{
						physical_cores_list[j].l1_tasks++;
					}
					else if(type == 2)
					{
						physical_cores_list[j].l2_tasks++;
					}
					cpumask_set_cpu(j, &mask);
					cpumask_set_cpu(j+20, &mask);
					sched_setaffinity(tempTask->pid, &mask);
					//tempTask->lbalancing.last_cpu = currCpu;
					//tempTask->lbalancing.migrated_recent = 1;
					printk(KERN_INFO "Sackstat : Current Task ->  Task_Name :%-15s  Task_ID :%d Cpu:%d Instructions:%llu  Cycles:%llu   MemoryOps:%llu  \n References:%llu  Misses:%llu Switches:%llu Weight:%d Moving Task from %d to %d \n",tempTask->comm ,tempTask->pid , currTask->current_cpu , tempTask->lbalancing.instructions_retired_saved , tempTask->lbalancing.total_cycles_saved , tempTask->lbalancing.memory_ops_saved , tempTask->lbalancing.llc_references_saved , tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migratable,currCpu,j);
					physical_cores_list[j].mem_total++;
					physical_cores_list[j].total++;
					break;
				}
			}

			if(currTask != NULL)
			{
				put_task_struct(currTask->lb_task_struct);
				kfree(currTask);
				currTask = NULL;
			}
		}
	}

	for(i = 0; i < 20; i+=2)
	{
		if(physical_cores_list[i].total == 0)
		{

			currTask = NULL;
			tempTask = NULL;

			for(j = 0; j < 20; j+=2)
			{
				if(i==j)
				{
					continue;
				}

				if(physical_cores_list[j].total >= 2 && physical_cores_list[j].core_tasks >= 1)
				{
					cpumask_clear(&mask);
					currTask = physical_cores_list[j].task_list_coreBound;
					physical_cores_list[j].task_list_coreBound = currTask->next;
					tempTask = currTask->lb_task_struct;
					physical_cores_list[j].core_tasks--;
					physical_cores_list[j].total--;
					if(tempTask == NULL || tempTask->state != TASK_RUNNING)
					{
						put_task_struct(tempTask);
						if(currTask != NULL)
						{
							kfree(currTask);
							currTask = NULL;
						}
						continue;
					}
					cpumask_set_cpu(i, &mask);
					cpumask_set_cpu(i+20, &mask);
					sched_setaffinity(tempTask->pid, &mask);
					printk(KERN_INFO "Sackstat : Core Task Balancing->  Task_Name :%-15s  Task_ID :%d Cpu:%d Instructions:%llu  Cycles:%llu   MemoryOps:%llu  \n References:%llu  Misses:%llu Switches:%llu Weight:%d Moving Task from %d to %d \n",tempTask->comm ,tempTask->pid , currTask->current_cpu , tempTask->lbalancing.instructions_retired_saved , tempTask->lbalancing.total_cycles_saved , tempTask->lbalancing.memory_ops_saved , tempTask->lbalancing.llc_references_saved , tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migratable,j,i);
					tempTask->lbalancing.weight_p = 31;
	                                tempTask->lbalancing.maskSet = 0;
					//cpumask_setall(&mask);
					//cpumask_copy(&tempTask->cpus_allowed,&mask);
					physical_cores_list[i].core_tasks++;
					physical_cores_list[i].total++;
		                        if(currTask != NULL)
                                        {
						put_task_struct(currTask->lb_task_struct);
                                                kfree(currTask);
                                                currTask = NULL;
                                        }
					break;
				}
			}
		}
	}

	currTask = NULL;
	tempTask = NULL;

	for(i = 0; i < 20; i+=2)
        {
                if(physical_cores_list[i].total == 1 && physical_cores_list[i].l2_tasks == 1)
                {
                        for(j = 0; j < 20; j+=2)
                        {
                                if(i==j)
                                {
                                        continue;
                                }

                                if(physical_cores_list[j].total >= 2 && physical_cores_list[j].core_tasks >= 2)
                                {
                                        cpumask_clear(&mask);
                                        currTask = physical_cores_list[j].task_list_coreBound;
                                        physical_cores_list[j].task_list_coreBound = currTask->next;
                                        physical_cores_list[j].core_tasks--;
                                        physical_cores_list[j].total--;
                                        tempTask = currTask->lb_task_struct;
                                       	if(tempTask == NULL || tempTask->state != TASK_RUNNING)
					{
						put_task_struct(tempTask);
						if(currTask != NULL)
						{
							kfree(currTask);
							currTask = NULL;
						}
						continue;
					}
					cpumask_set_cpu(i, &mask);
                                        cpumask_set_cpu(i+20, &mask);
                                        sched_setaffinity(tempTask->pid, &mask);
					printk(KERN_INFO "Sackstat : Core Task Balancing->  Task_Name :%-15s  Task_ID :%d Cpu:%d Instructions:%llu  Cycles:%llu   MemoryOps:%llu  \n References:%llu  Misses:%llu Switches:%llu Weight:%d Moving Task from %d to %d \n",tempTask->comm ,tempTask->pid , currTask->current_cpu , tempTask->lbalancing.instructions_retired_saved , tempTask->lbalancing.total_cycles_saved , tempTask->lbalancing.memory_ops_saved , tempTask->lbalancing.llc_references_saved , tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migratable,j,i);
                                        tempTask->lbalancing.weight_p = 31;
	                                tempTask->lbalancing.maskSet = 0; 
					//cpumask_setall(&mask);
                                        //cpumask_copy(&tempTask->cpus_allowed,&mask);
                                        physical_cores_list[i].core_tasks++;
                                        physical_cores_list[i].total++;
			                if(currTask != NULL)
                                        {
                        			put_task_struct(currTask->lb_task_struct);
			                        kfree(currTask);
                                                currTask = NULL;
                                        }
					break;
                                }
                        }
                }
        }

        currTask = NULL;
        tempTask = NULL;

	for(i = 0; i < 20; i+=2)
        {
                if(physical_cores_list[i].total == 1 && physical_cores_list[i].mem_total == 1)
                {
                        for(j = 0; j < 20; j+=2)
                        {
                                if(i==j)
                                {
                                        continue;
                                }

                                if(physical_cores_list[j].total >= 2 && physical_cores_list[j].core_tasks >= 2)
                                {
                                        cpumask_clear(&mask);
                                        currTask = physical_cores_list[j].task_list_coreBound;
                                        physical_cores_list[j].task_list_coreBound = currTask->next;
                                        physical_cores_list[j].core_tasks--;
                                        physical_cores_list[j].total--;
                                        tempTask = currTask->lb_task_struct;
                                       	if(tempTask == NULL || tempTask->state != TASK_RUNNING)
					{
						put_task_struct(tempTask);
						if(currTask != NULL)
						{
							kfree(currTask);
							currTask = NULL;
						}
						continue;
					}
					cpumask_set_cpu(i, &mask);
                                        cpumask_set_cpu(i+20, &mask);
                                        sched_setaffinity(tempTask->pid, &mask);
					printk(KERN_INFO "Sackstat : Core Task Balancing->  Task_Name :%-15s  Task_ID :%d Cpu:%d Instructions:%llu  Cycles:%llu   MemoryOps:%llu  \n References:%llu  Misses:%llu Switches:%llu Weight:%d Moving Task from %d to %d \n",tempTask->comm ,tempTask->pid , currTask->current_cpu , tempTask->lbalancing.instructions_retired_saved , tempTask->lbalancing.total_cycles_saved , tempTask->lbalancing.memory_ops_saved , tempTask->lbalancing.llc_references_saved , tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migratable,j,i);
                                        tempTask->lbalancing.weight_p = 31;
	                                tempTask->lbalancing.maskSet = 0; 
					//cpumask_setall(&mask);
                                        //cpumask_copy(&tempTask->cpus_allowed,&mask);
                                        physical_cores_list[i].core_tasks++;
                                        physical_cores_list[i].total++;
			                if(currTask != NULL)
                                        {
                        			put_task_struct(currTask->lb_task_struct);
			                        kfree(currTask);
                                                currTask = NULL;
                                        }
					break;
                                }
                        }
                }
        }

        currTask = NULL;
        tempTask = NULL;

	dealloc_lists();
	return;
}

/*
void balance_load(void)
{
	int i , j ;

	int type=0,currCpu=0;

	struct tasks_info *currTask = NULL;

	struct task_struct *tempTask;

	struct cpumask mask;

	for(i =0; i < 12; i+=2)
	{

		printk(KERN_INFO "Sackstat ******* Start ********\n\n\n");
		currTask = physical_cores_list[i].task_list_memBound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : Memory Task ->Task_Name:%-15s  Task_ID:%5u  Cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t LLC:%llu Misses:%llu Swit:%llu  Mig:%d  MemTrig:%d  MemClass:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		currTask = physical_cores_list[i].task_list_l3Bound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : L3 Task ->Task_Name:%-15s  Task_ID:%5u  Cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t LLC:%llu Misses:%llu Swit:%llu  Mig:%d  MemTrig:%d  MemClass:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		currTask = physical_cores_list[i].task_list_l2Bound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : L2 Task ->Task_Name:%-15s  Task_ID:%5u  Cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t LLC:%llu Misses:%llu Swit:%llu  Mig:%d  MemTrig:%d  MemClass:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		currTask = physical_cores_list[i].task_list_coreBound;

		while(currTask != NULL)
		{
			tempTask = currTask->lb_task_struct;
			if(tempTask != NULL && tempTask->state == TASK_RUNNING)
			{
				printk(KERN_INFO "Sackstat : Core Task ->Task_Name:%-15s  Task_ID:%5u  Cpu:%d  ActualCpu:%d  Ins:%llu  Cycles:%llu  MemOps:%llu\n\t\t LLC:%llu Misses:%llu Swit:%llu  Mig:%d  MemTrig:%d  MemClass:%d  CoreTrig:%d  CoreClass:%d\n",tempTask->comm, tempTask->pid, currTask->current_cpu, tempTask->lbalancing.cpu, tempTask->lbalancing.instructions_retired_saved, tempTask->lbalancing.total_cycles_saved, tempTask->lbalancing.memory_ops_saved, tempTask->lbalancing.llc_references_saved, tempTask->lbalancing.llc_misses_saved, tempTask->lbalancing.context_switches, tempTask->lbalancing.migrations, tempTask->lbalancing.memory_trigger_count, tempTask->lbalancing.memory_phase_count, tempTask->lbalancing.core_trigger_count, tempTask->lbalancing.core_phase_count);
			}
			currTask = currTask->next;
		}

		printk(KERN_INFO "\n\n\n Sackstat ******** End ********\n\n\n");

		//printk(KERN_INFO "Sackstat : Total Core Bound in Physical Core in Core %d ->  %d \n",i,physical_cores_list[i].core_tasks);
	}
    
	currTask = NULL;
	tempTask = NULL;

	for(i = 0; i < 12; i+=2)
	{
		while(physical_cores_list[i].mem_total > 1)
		{
			if(physical_cores_list[i].mem_tasks > 0 )
			{
			}
	}

}

void migrate_threads(void)
{
    struct physical_core_info *temp;

//    struct tasks_info *currTask;

  //  struct cpumask mask;

  //  struct task_struct *tempStruct;

    temp = physical_cores_list[0].physical_list_head;

    printk(KERN_INFO "MIGRATING \n");

    while(temp != NULL)
    {
        if(temp->need_resched == 1)
        {
            if(logical_cores_info[temp->core_id].need_resched == 1)
            {
                    if(logical_cores_info[temp->core_id].mem_total > 0)
                    {
                        if(logical_cores_info[temp->core_id].mem_tasks > 0)
                        {
                            currTask = logical_cores_info[temp->core_id].task_list_memBound;

                            while(currTask != NULL)
                            {
                                tempStruct = currTask->lb_task_struct;

                                if(currTask->need_resched == 1 && tempStruct != NULL &&tempStruct->state == TASK_RUNNING)
                                {
                                    cpumask_clear(&mask);

                                    cpumask_set_cpu(currTask->new_cpu, &mask);

                                    sched_setaffinity(tempStruct->pid,&mask);
                                }
				tempStruct->lbalancing.stats_collected = 0;
                                currTask = currTask->next;
                            }
                        }

                        if(logical_cores_info[temp->core_id].l3_tasks > 0)
                        {
                            currTask = logical_cores_info[temp->core_id].task_list_l3Bound;

                            while(currTask != NULL)
                            {
                                tempStruct = currTask->lb_task_struct;

                                if(currTask->need_resched == 1 && tempStruct != NULL && tempStruct->state == TASK_RUNNING)
                                {
                                    cpumask_clear(&mask);

                                    cpumask_set_cpu(currTask->new_cpu, &mask);

                                    sched_setaffinity(tempStruct->pid,&mask);
                                }
				tempStruct->lbalancing.stats_collected = 0;
                                currTask = currTask->next;
                            }
                        }

                        if(logical_cores_info[temp->core_id].l2_tasks > 0)
                        {
                            currTask = logical_cores_info[temp->core_id].task_list_l2Bound;

                            while(currTask != NULL)
                            {
                                tempStruct = currTask->lb_task_struct;

                                if(currTask->need_resched == 1 && tempStruct != NULL && tempStruct->state == TASK_RUNNING)
                                {
                                    cpumask_clear(&mask);

                                    cpumask_set_cpu(currTask->new_cpu, &mask);

                                    sched_setaffinity(tempStruct->pid,&mask);
                                }
				tempStruct->lbalancing.stats_collected = 0;
                                currTask = currTask->next;
                            }
                        }
                    }

                    if(logical_cores_info[temp->core_id].core_tasks > 0)
                    {
                        currTask = logical_cores_info[temp->core_id].task_list_coreBound;

                        while(currTask != NULL)
                        {
                                tempStruct = currTask->lb_task_struct;

                                if(currTask->need_resched == 1 && tempStruct != NULL && tempStruct->state == TASK_RUNNING)
                                {
                                    cpumask_clear(&mask);

                                    cpumask_set_cpu(currTask->new_cpu, &mask);

                                    sched_setaffinity(tempStruct->pid,&mask);
                                }
			    tempStruct->lbalancing.stats_collected = 0;
                            currTask = currTask->next;
                        }
                    }
            }
            else if(logical_cores_info[temp->core_id + 12].need_resched == 1)
            {
                    if(logical_cores_info[temp->core_id + 12].mem_total > 0)
                    {
                        if(logical_cores_info[temp->core_id + 12].mem_tasks > 0)
                        {
                            currTask = logical_cores_info[temp->core_id + 12].task_list_memBound;

                            while(currTask != NULL)
                            {
                                tempStruct = currTask->lb_task_struct;

                                if(currTask->need_resched == 1 && tempStruct != NULL && tempStruct->state == TASK_RUNNING)
                                {
                                    cpumask_clear(&mask);

                                    cpumask_set_cpu(currTask->new_cpu, &mask);

                                    sched_setaffinity(tempStruct->pid,&mask);
                                }
				tempStruct->lbalancing.stats_collected = 0;
                                currTask = currTask->next;
                            }
                        }

                        if(logical_cores_info[temp->core_id + 12].l3_tasks > 0)
                        {
                            currTask = logical_cores_info[temp->core_id + 12].task_list_l3Bound;

                            while(currTask != NULL)
                            {
                                tempStruct = currTask->lb_task_struct;

                                if(currTask->need_resched == 1 && tempStruct != NULL && tempStruct->state == TASK_RUNNING)
                                {
                                    cpumask_clear(&mask);

                                    cpumask_set_cpu(currTask->new_cpu, &mask);

                                    sched_setaffinity(tempStruct->pid,&mask);
                                }
				tempStruct->lbalancing.stats_collected = 0;
                                currTask = currTask->next;
                            }
                        }

                        if(logical_cores_info[temp->core_id + 12].l2_tasks > 0)
                        {
                            currTask = logical_cores_info[temp->core_id + 12].task_list_l2Bound;

                            while(currTask != NULL)
                            {
                                tempStruct = currTask->lb_task_struct;

                                if(currTask->need_resched == 1 && tempStruct != NULL && tempStruct->state == TASK_RUNNING)
                                {
                                    cpumask_clear(&mask);

                                    cpumask_set_cpu(currTask->new_cpu, &mask);

                                    sched_setaffinity(tempStruct->pid,&mask);
                                }
				tempStruct->lbalancing.stats_collected = 0;
                                currTask = currTask->next;
                            }
                        }
                    }

                    if(logical_cores_info[temp->core_id + 12].core_tasks > 0)
                    {
                        currTask = logical_cores_info[temp->core_id + 12].task_list_coreBound;

                        while(currTask != NULL)
                        {
                            tempStruct = currTask->lb_task_struct;

                            if(currTask->need_resched == 1 && tempStruct != NULL && tempStruct->state == TASK_RUNNING)
                            {
                                cpumask_clear(&mask);

                                cpumask_set_cpu(currTask->new_cpu, &mask);

                                sched_setaffinity(tempStruct->pid,&mask);
                            }
			    tempStruct->lbalancing.stats_collected = 0;
                            currTask = currTask->next;
                        }
                    }
            }
        }

        temp = temp->next;
    }
	dealloc_lists(1);
    return;
}
*/

void dealloc_lists(void)
{
	struct tasks_info *tempTask , *tempTask2;

	//struct task_struct *currTask;

	int i;

	for(i = 0; i < 2; i++)
	{
		sockets_info[i].total = 0;
		sockets_info[i].mem_total = 0;
		sockets_info[i].mem_tasks = 0;
		sockets_info[i].l3_tasks = 0;
		sockets_info[i].l2_tasks = 0;
		sockets_info[i].l1_tasks = 0;
		sockets_info[i].core_tasks = 0;
	}

	for(i = 0; i < 20; i++)
	{
		if(physical_cores_list[i].task_list_memBound != NULL)
		{
			tempTask = physical_cores_list[i].task_list_memBound;

			while(tempTask != NULL)
			{
				tempTask2 = tempTask;
				tempTask = tempTask->next;
				put_task_struct(tempTask2->lb_task_struct);
				kfree(tempTask2);
				tempTask2 = NULL;
			}

			physical_cores_list[i].task_list_memBound = NULL;
		}

		if(physical_cores_list[i].task_list_l3Bound != NULL)
		{
			tempTask = physical_cores_list[i].task_list_l3Bound;

			while(tempTask != NULL)
			{
				tempTask2 = tempTask;
				tempTask = tempTask->next;
				put_task_struct(tempTask2->lb_task_struct);
				kfree(tempTask2);
				tempTask2 = NULL;
			}

			physical_cores_list[i].task_list_l3Bound = NULL;
		}

		if(physical_cores_list[i].task_list_l2Bound != NULL)
		{
			tempTask = physical_cores_list[i].task_list_l2Bound;

			while(tempTask != NULL)
			{
				tempTask2 = tempTask;
				tempTask = tempTask->next;
				put_task_struct(tempTask2->lb_task_struct);
				kfree(tempTask2);
				tempTask2 = NULL;
			}

			physical_cores_list[i].task_list_l2Bound = NULL;
		}

		if(physical_cores_list[i].task_list_l1Bound != NULL)
                {
                        tempTask = physical_cores_list[i].task_list_l1Bound;

                        while(tempTask != NULL)
                        {
                                tempTask2 = tempTask;
                                tempTask = tempTask->next;
				put_task_struct(tempTask2->lb_task_struct);
                                kfree(tempTask2);
                                tempTask2 = NULL;
                        }

                        physical_cores_list[i].task_list_l1Bound = NULL;
                }

		if(physical_cores_list[i].task_list_coreBound != NULL)
		{
			tempTask = physical_cores_list[i].task_list_coreBound;

			while(tempTask != NULL)
			{
				tempTask2 = tempTask;
				tempTask = tempTask->next;
				put_task_struct(tempTask2->lb_task_struct);
				kfree(tempTask2);
				tempTask2 = NULL;
			}

			physical_cores_list[i].task_list_coreBound = NULL;
		}

		physical_cores_list[i].total = 0;
		physical_cores_list[i].mem_total = 0;
		physical_cores_list[i].mem_tasks = 0;
		physical_cores_list[i].l3_tasks = 0;
		physical_cores_list[i].l2_tasks = 0;
		physical_cores_list[i].l1_tasks = 0;
		physical_cores_list[i].core_tasks = 0;
	}
	return;
}

// Function executed by kernel thread
static int thread_fn(void *unused)
{
	// Allow the SIGKILL signal

	allow_signal(SIGKILL);

	while (!kthread_should_stop())
	{
		ssleep(sleep_time);

		sackstat_resched();

		// Check if the signal is pending
		if (signal_pending(thread_st))
			break;
	}

	do_exit(0);
	return 0;
}


static int __init msr_init(void)
{
	int current_cpu;
	struct cpumask mask;

	int i = 0;

	while(i < 40)
	{
		current_cpu = current->cpu;

		if(current_cpu != i)
		{
			cpumask_clear(&mask);
			cpumask_set_cpu(i, &mask);
			sched_setaffinity(current->pid, &mask);
			schedule();
		}

		native_write_msr(0x38f,0x00,0x00);
		writemsr(IA32_FIXED_CTR0, NONE);
		writemsr(IA32_FIXED_CTR1, NONE);
		writemsr(IA32_FIXED_CTR2, NONE);
		writemsr(IA32_PMC0, 0);
		writemsr(IA32_PMC1, 0);
		writemsr(IA32_PMC2, 0);
		writemsr(IA32_PMC3, 0);
		writemsr(IA32_PERFEVTSEL0, 0x4181D0);
		writemsr(IA32_PERFEVTSEL1, 0x4182D0);
		writemsr(IA32_PERFEVTSEL2, 0x41E124);
		writemsr(IA32_PERFEVTSEL3, 0x41E224);
		native_write_msr(0x38d,0x333,0x00);
		native_write_msr(0x38f,0x0f,0x07);

		i++;
	}

	cpumask_setall(&mask);
        cpumask_copy(&current->cpus_allowed,&mask);

	//printk(KERN_INFO "Creating Thread\n");

	//Create the  kernel thread with name 'mythread'

	thread_st = kthread_create(thread_fn, NULL,"mythread");

	if (thread_st)
	{

		for(i = 0; i < 2; i++)
		{
			sockets_info[i].total = 0;
			sockets_info[i].mem_total = 0;
			sockets_info[i].mem_tasks = 0;
			sockets_info[i].l3_tasks = 0;
			sockets_info[i].l2_tasks = 0;
			sockets_info[i].l1_tasks = 0;
			sockets_info[i].core_tasks = 0;
		}

		for(i = 0; i < 20; i++)
		{
			physical_cores_list[i].task_list_memBound = NULL;
			physical_cores_list[i].task_list_l3Bound = NULL;
			physical_cores_list[i].task_list_l2Bound = NULL;
			physical_cores_list[i].task_list_l1Bound = NULL;
			physical_cores_list[i].task_list_coreBound = NULL;
			physical_cores_list[i].total = 0;
			physical_cores_list[i].mem_total = 0;
			physical_cores_list[i].mem_tasks = 0;
			physical_cores_list[i].l3_tasks = 0;
			physical_cores_list[i].l2_tasks = 0;
			physical_cores_list[i].l1_tasks = 0;
			physical_cores_list[i].core_tasks = 0;
		}
		wake_up_process(thread_st);
	}
	else
	{
		printk(KERN_ERR "Thread creation failed\n");
	}

	return 0;
}


static void __exit msr_cleanup(void)
{
	int current_cpu;
	struct cpumask mask;

	int i = 0;

	while(i < 40)
	{
		current_cpu = current->cpu;

		if(current_cpu != i)
		{
			cpumask_clear(&mask);
			cpumask_set_cpu(i, &mask);
			sched_setaffinity(current->pid, &mask);
			schedule();
		}

		native_write_msr(0x38f,0x00,0x00);
		native_write_msr(0x38d,0x00,0x00);
		writemsr(IA32_FIXED_CTR0, NONE);
		writemsr(IA32_FIXED_CTR1, NONE);
		writemsr(IA32_PMC0, 0);
		writemsr(IA32_PMC1, 0);
		writemsr(IA32_PMC2, 0);
		writemsr(IA32_PMC3, 0);
		i++;
	}

	printk(KERN_INFO "Cleaning Up\n");

	ssleep(2);
	dealloc_lists();

	if (thread_st)
	{
		kthread_stop(thread_st);
		printk(KERN_INFO "Thread stopped");
	}

}

module_init(msr_init);
module_exit(msr_cleanup);
